### Js Libraries ###

* Vue.js

### Frontend Assets Utils ###

* iconmoon - Packetize icons
* [avatars adorable](http://avatars.adorable.io/) - Avatar Generator
* [unsplash](https://unsplash.com/) - Free use Photos Library

### Podcast ###

* js Party
* Software Engineering Radio

# Backend #

### Frameworks ###

* Laravel
